﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    public GameObject player;

    private void LateUpdate()
    {
        Vector3 pos = new Vector3(
            player.transform.position.x,
            player.transform.position.y,
            transform.position.z
        );
        transform.position = pos;
    }
}
