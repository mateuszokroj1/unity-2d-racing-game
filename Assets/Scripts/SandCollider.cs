﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandCollider : MonoBehaviour
{
    public Player player;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
            player.LimitOn();
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
            player.LimitOff();
    }
}
