﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;

public class AI : MonoBehaviour, ICar
{
    private Rigidbody2D rg;
    private Vector2 V;
    // Start is called before the first frame update
    void Start()
    {
        rg = gameObject.GetComponent<Rigidbody2D>();
    }


    // Update is called once per frame
    void Update()
    {
        
        rg.velocity = VectorTrygonometry.Rotate(V, rg.rotation - 90);
    }

    public void Accelerate()
    {
        if(V.magnitude < 7 )
            V = V + new Vector2(0.15f, 0);
    }
    public void Decelerate()
    {
        if(V.magnitude < 4)
            V = V - new Vector2(0.3f, 0);
    }
    public void Left()
    {
        if(V.x < 0 || V.y < 0)
            rg.rotation -= 2f * V.magnitude / 3f;
        else
            rg.rotation += 2f * V.magnitude / 3f;
    }
    public void Right()
    {
        if(V.x < 0 || V.y < 0)
            rg.rotation += 2f * V.magnitude / 4f;
        else
            rg.rotation -= 2f * V.magnitude / 4f;
    }

    public void LimitOff() => throw new NotImplementedException();
    public void LimitOn() => throw new NotImplementedException();
    public void Stop() => V = Vector2.zero;
}
