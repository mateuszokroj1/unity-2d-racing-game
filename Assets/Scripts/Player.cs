﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;

public class Player : MonoBehaviour, ICar
{
    private Rigidbody2D rg;
    private Vector2 V;
    private bool limit = false;

    public void Stop() => V = Vector2.zero;
    public void LimitOn() => limit = true;
    public void LimitOff() => limit = false;

    public void Accelerate()
    {
        if((limit & V.magnitude <= 1.5) || (!limit & V.magnitude < 7))
            V = V + new Vector2(0.15f, 0);            
    }
    public void Decelerate()
    {
        if ((limit & V.magnitude < 1.5) || (!limit & V.magnitude < 4))
            V = V - new Vector2(0.3f, 0);
    }
    public void Left()
    {
        if (V.x < 0 || V.y < 0)
            rg.rotation -= 2f * V.magnitude / 3f;
        else
            rg.rotation += 2f * V.magnitude / 3f;
    }
    public void Right()
    {
        if (V.x < 0 || V.y < 0)
            rg.rotation += 2f * V.magnitude / 4f;
        else
            rg.rotation -= 2f * V.magnitude / 4f;
    }

    // Start is called before the first frame update
    void Start()
    {
        rg = gameObject.GetComponent<Rigidbody2D>();
        V = new Vector2(0, 0);
    }

    public void OnCollisionEnter2D(Collision2D collision) => V = Vector2.zero;

    // Update is called once per frame
    void Update()
    {
        // Speed control
        if (!Input.GetKey(KeyCode.UpArrow) & !Input.GetKey(KeyCode.DownArrow))
        { // No key = stopping
            if (V.magnitude >= 0.006f)
                if(limit)
                    if(V.x > 0)
                        V -= new Vector2(0.1f, 0);
                    else
                        V += new Vector2(0.1f, 0);
                else if(V.x > 0)
                    V -= new Vector2(0.05f, 0);
                else
                    V += new Vector2(0.05f, 0);
            if (V.magnitude < 0.005f) // Autostop
                V = new Vector2(0, 0);
        }
        else if (Input.GetKey(KeyCode.UpArrow) & !Input.GetKey(KeyCode.DownArrow))
            Accelerate();
        else if (!Input.GetKey(KeyCode.UpArrow) & Input.GetKey(KeyCode.DownArrow))
            Decelerate();
            
        // Saves changes to vector V and copy rotated V to RigidBody
        rg.velocity = VectorTrygonometry.Rotate(V, rg.rotation-90);

        // Rotation
        if (Input.GetKey(KeyCode.LeftArrow) & !Input.GetKey(KeyCode.RightArrow))
            Left();
        else if (!Input.GetKey(KeyCode.LeftArrow) & Input.GetKey(KeyCode.RightArrow))
            Right();
    }
    
}

public static class VectorTrygonometry
{
    /// <summary>
    /// Rotate vector in angle (deg)
    /// </summary>
    /// <param name="vector">2D vector</param>
    /// <param name="angle">Angle in degrees</param>
    public static Vector2 Rotate(Vector2 vector, float angle)
    {
        angle = angle * Mathf.Deg2Rad; // angle = angle*Math.PI/180
        return new Vector2(vector.x * Mathf.Cos(angle) - vector.y * Mathf.Sin(angle),
                           vector.x * Mathf.Sin(angle) + vector.y * Mathf.Cos(angle));
    }
}
