﻿namespace Assets.Scripts
{
    public interface ICar
    {
        void Accelerate();
        void Decelerate();
        void Left();
        void Right();
        void Stop();
        void LimitOn();
        void LimitOff();
    }
}
